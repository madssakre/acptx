load 300b1;
load 300b0_shimmed;

resXY = 2; resZ = 2;

temp = padarray(b1,[0,11,0,0]);
b1 = permute(temp(:,1:resXY:end,1:resXY:end,1:resZ:end),[2,3,4,1]) * 100000;
b1 = b1.*repmat(exp(-1i*angle(b1(:,:,:,1))),[1 1 1 8]);
temp = padarray(b0,[11,0,0]);
b0 = temp(1:resXY:end,1:resXY:end,1:resZ:end);

mask = sum(abs(b1),4) > 0;

maps.mask = mask;
maps.b0 = b0;
maps.b1 = b1;

maps.fov = [18 18 12];

save 3Db1b0maps maps
