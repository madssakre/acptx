% Script to design a 2D spokes compressed pulse (8Ch)
% Copyright Zhipeng Cao and Will Grissom, Vanderbilt University, 2015.

addpath util

prbp.delta_tip = 180; % flip angle, deg
nPulse = 7; % number of subpules
if ~exist('Ncred','var')
    Ncred = 2; % number of compressed Tx channels
end
if ~exist('compApproach','var')
    compApproach = 'arrayComp';
end
if ~exist('coilMappingType','var')
    coilMappingType = 'consecutive'; % 'consecutive' or 'interleaved'
end

%%
% Load B1 and b0 Fields, select middle slice for our design
load ../ktpoints/3Db1b0maps

Nc = size(maps.b1,4);

maps.b1 = squeeze(maps.b1(:,:,floor(size(maps.b1,3)/2),:));
maps.b0 = squeeze(maps.b0(:,:,floor(size(maps.b0,3)/2)));
maps.mask = squeeze(maps.mask(:,:,floor(size(maps.mask,3)/2)));
maps.fov = maps.fov(1:2);
% set initial target phase to quad mode phase
bcb1 = 0;
for ii = 1:Nc
  bcb1 = bcb1 + maps.b1(:,:,ii)*exp(1i*(ii-1)*2*pi/Nc).*exp(-1i*angle(maps.b1(:,:,1)));
end
maps.phsinit = angle(bcb1);

%% Fill design parameter structures
prbp.Nrungs = nPulse;
prbp.kmaxdistance = [Inf Inf]; % maximum k-space radius for spoke locations
prbp.dt = 6.4e-6; % dwell time, seconds
prbp.dimxy = size(maps.mask); % dimensions of b1/b0/mask matrices
prbp.maxpulsedur = Inf;            % maximum pulse duration in ms

prbp.filtertype = 'Lin phase';
prbp.trajres = 8;                   % maximum spatial frequency of OMP search grid (cm)
                                    % WAG: This is an IMPORTANT parameter to the success of the designs. 
                                    % I am not sure why exactly, but my guess is that when it is too large,
                                    % rapid variations in the total B1+ patterns are created by SVD, which lead to 
                                    % the selection of high-frequency spoke locations by ilgreedylocal, 
                                    % and high peak RF power results in the DC spoke. 
                                    % When the pulse is then input to lt_oc, it turns out due to Bloch non-linearity that
                                    % those locations are not so good, so that degrades the final performance considerably.
                                    % If it is too big, then all the spokes
                                    % go right in the center of k-space,
                                    % limiting B1 inhomogeneity mitigation
                                    % too much.
                                    % trajres = 8 cm was a good balance
prbp.flyback = 0;                   % No flyback -> Shorter pulse.
prbp.sameflyback = 1;
prbp.z_tbw = 2;
prbp.z_pulsetype = 'lin';
prbp.dthick = 3;                    % Slice Thickness, mm

prbp.d1 = 0.01; prbp.d2 = 0.01;     % SLR Parameters for RF subpulse shape
prbp.np = 128;                      % RF subpulse length

prbp.pulse_func = 'ref'; % refocusing
prbp.ltgradupdate = 1;
prbp.beta = 10^-1;          % RF Tikhonov regularization parameter
prbp.betaadjust = 1;
prbp.maxgradamp = 3;        % g/cm, max grad during RF
prbp.gmax = 4;              % g/cm, max grad for rewinders
prbp.maxgradslew = 8000;    % g/cm/s
prbp.dorflim = 0;
prbp.rflim = 0.1;
prbp.coilMapping = [];      % to be filled in later only if Floeser

algp.phsopt = 'independent';% Independent target phase updates across frequencies
algp.nthreads = 4;          % number of compute threads (for mex only)
algp.compmethod = 'mex';    % use mex functions for bloch sim and derivative calcs
algp.maxcgiters = 10;       % maximum RF/gradient CG iters
algp.cgstopthresh = 0.9999; % CG consecutive cost stopping threshold
algp.mainiters = 10;        % maximum RF/gradient CG iters
algp.maintol = 0.9999;      % CG consecutive cost stopping threshold
algp.RFal = 0.3;            % RF backtracking line search al
algp.RFbl = 0.3;            % RF backtracking line search bl
algp.RFmintl = 10^-10;      % RF backtracking line search min tl to consider
algp.gradal = 0.3;          % Gradient backtracking line search al
algp.gradbl = 0.3;          % Gradient backtracking line search bl
algp.gradmintl = 10^-20;    % Gradient backtracking line search min tl to consider
algp.ncgiters = 3;          % # CG iters per small-tip RF update
algp.use_lt_oc_zc = 0;      % use Zhipeng Cao's version of Optimal Control, or the old version

switch compApproach
    case 'allChannels'  % Design with full channel
        
        disp 'Running all-channels design'
        
        prbp.Ncred = Inf;
        [all_images, wvfrms, aout, bout, err] = design_spokes(prbp,algp,maps);
        rfw = wvfrms.rf;
        compWts = eye(Nc);
        
    case 'arrayComp'
        
        prbp.Ncred = Ncred; % number of compressed channels
        printf('Running compressed %d-channel design\n',prbp.Ncred);
        
        % do a compressed design
        [all_images, wvfrms, aout, bout, err, nSVT] = design_spokes(prbp,algp,maps);
        rfw = wvfrms.rf;
        compWts = wvfrms.compWts;
        
    case 'oneToN' % 1->N splitting with optimized coil weights, a la Floeser et al
        
        prbp.Ncred = 1; % one channel per split
        switch coilMappingType
         case 'consecutive'
          prbp.coilMapping = reshape(1:Nc,[Nc/Ncred Ncred]); % split array into Nc/Nred consecutive elements
         case 'interleaved'
          prbp.coilMapping = reshape(1:Nc,[Ncred Nc/Ncred]).'; % split array into Nc/Nred interleaved elements
        end
        
        printf('Running Floeser''s %d-channel design\n',Ncred);
        
        % do a compressed design
        [all_images, wvfrms, aout, bout, err] = design_spokes(prbp,algp,maps);
        rfw = wvfrms.rf;
        compWts = wvfrms.compWts;
        
    case 'quadComp'  % Quardrature-based every-other-coil combination
        
        prbp.Ncred = Ncred; % number of compressed channels
        printf('Running quadrature-compressed %d-channel design\n',prbp.Ncred);
        
        % build coil compression matrix
        quadWts = exp(1i*(0:Nc-1)*2*pi/Nc);
        compWts = zeros(Nc,prbp.Ncred);
        switch coilMappingType
         case 'interleaved'
          for ii = 1:prbp.Ncred
            compWts(ii:prbp.Ncred:end,ii) = quadWts(ii:prbp.Ncred:end);
          end
         case 'consecutive'
          for ii = 1:prbp.Ncred
            compWts((ii-1)*Nc/prbp.Ncred+1:ii*Nc/prbp.Ncred,ii) = quadWts((ii-1)*Nc/prbp.Ncred+1:ii*Nc/prbp.Ncred);
          end
        end
        b1Comp = permute(maps.b1,[3 1 2]);
        b1Comp = b1Comp(:,:).';
        b1Comp = reshape(b1Comp*compWts,[prbp.dimxy prbp.Ncred]);
        %figure(1); im(b1Comp); colormap jet;
        
        % And then use pre-compressed B1 to design pulses, without compression
        maps.b1 = b1Comp;
        prbp.Ncred = Inf;
        [all_images, wvfrms, aout, bout, err] = design_spokes(prbp,algp,maps);
        % Convert back to full channel pulses for RF power comparison
        rfw = (compWts*wvfrms.rf.').';
        
    case 'modes'  % Quadrature-based every-other-coil combination
        
        prbp.Ncred = Ncred; % number of compressed channels
        printf('Running mode-compressed %d-channel design\n',prbp.Ncred);
        
        % build coil compression matrix
        compWts = exp(1i*(0:Nc-1)'*(1:prbp.Ncred)*2*pi/Nc);
        
        % apply coil compression matrix
        b1Comp = permute(maps.b1,[3 1 2]);
        b1Comp = b1Comp(:,:).';
        b1Comp = reshape(b1Comp*compWts,[prbp.dimxy prbp.Ncred]);
        
        %figure(1); im(b1Comp); colormap jet;

        % And then use pre-compressed B1 to design pulses, without compression
        maps.b1 = b1Comp;
        prbp.Ncred = Inf;
        [all_images, wvfrms, aout, bout, err] = design_spokes(prbp,algp,maps);
        % Convert back to full channel pulses for RF power comparison
        rfw = (compWts*wvfrms.rf.').';
        
    case 'b1SVTComp' % B1-map singular value truncation
        
        prbp.Ncred = Ncred; % number of compressed channels
        printf('Running B1 SVT-compressed %d-channel design\n',prbp.Ncred);

        for n = 1:Nc
            tmp = maps.b1(:,:,n);
            b1tmp(n,:) = tmp(maps.mask);
        end
        [u,s,v] = svd(b1tmp,'econ');
        compWts = u(:,1:prbp.Ncred)';
        
        b1tmp = compWts * b1tmp; % Right
        
        b1Comp = [];
        for n = 1:prbp.Ncred
            b1Comp(:,:,n) = embed(b1tmp(n,:).',maps.mask);
        end
        %figure(1); im(b1Comp); colormap jet;
        
        % And then use combined B1 maps to design pulse
        load b1SVTCompInitPhs % better phase initialization for B1SVT
        maps.phsinit = phsinit;
        maps.b1 = b1Comp;
        prbp.Ncred = Inf;
        [all_images, wvfrms, aout, bout, err] = design_spokes(prbp,algp,maps);
        rfw = wvfrms.rf * compWts;
        
end

farmse = sqrt(mean((asin(sqrt(abs(all_images.images(maps.mask))))*2/pi*180 - prbp.delta_tip).^2));
rfrms = norm(rfw);
fprintf('Flip angle RMSE: %.4f, RMS RF power: %.4f.\n\n',farmse,rfrms);

load ../ktpoints/300e
tmpSAR = 0 .* cond3D;
tmpP   = 0 .* cond3D;
for mm = 1:prbp.Nrungs
    tmpE = repmat(0 .* cond3D,[1,1,1,3]);
    for nn = 1:Nc
        tmpE = tmpE + e13D(:,:,:,:,nn) * rfw(mm,nn);
    end
    tmpP   = tmpP   + cond3D .* sum(abs(tmpE).^2,4);
    tmpSAR = tmpSAR + cond3D .* sum(abs(tmpE).^2,4) ./ dens3D;
end
tmpSAR(isnan(tmpSAR)) = 0;

SARave = SARavesphnp(dens3D, tmpSAR, 0.004, 0.004, 0.004, 10);

rftotal = sum(tmpP(:)) / 100000000;
maxsar = max(SARave(:));

fprintf('Total RF power deposition: %.4f, max 10g SAR: %.4f.\n\n', rftotal, maxsar);

