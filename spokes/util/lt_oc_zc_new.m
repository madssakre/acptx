function [rf,kpe,a,b,err] = lt_oc_zc_new(ad,bd,sens,xx,kinit,f0,Brf,prbp,algp,gpos,gfly,nramp,lambda,rf,mask,phsa,phsb);

disp('Designing large-tip pulses.')

gambar = 4257;             % gamma/2pi in Hz/T
gam = gambar*2*pi;         % gamma in radians/g

% calculate area of phase encodes from kpe
garea = diff([kinit;zeros(1,size(kinit,2))],1)/gambar;

Nrungs = size(garea,1);

Nc = size(sens,2); % number of tx channels
Ns = size(xx,1); % number of spatial locations
Nb = length(fb); % number of frequency bands

xxmb = repmat(xx,[Nb 1]);

compWts = eye(Nc); % compression weights

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RF basis for lt design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if prbp.z_tbw > 0
	[Brf,avgflip,rf] = genRFbasis(Nrungs,dt,rfss,sens,rf,d1,d2, ...
                              z_tbw,rampsamp,gpos,nramp,gfly);
  % convert Brf to vector for mex blochsim
  brfvec = [];
  for ii = 1:length(Brf)
      brfvec = [brfvec; Brf{ii}(:)];
      ntbrf(ii) = length(Brf{ii});
  end;
else % kT-points - the subpulses won't change
  % convert Brf to vector for mex blochsim
  brfvec = [];
  for ii = 1:length(rfss)
    brfvec = [brfvec;rfss{ii}];
    ntbrf(ii) = length(rfss{ii});
  end
  avgflip = [];
  Brf = rfss;
end

% get relationship between phase encodes and poly terms
[Ag,Bg,Ans,Bns] = get_ab_to_g(Nrungs);

% target phase variables and phase-modified patterns
if isempty(phsa)
  switch phsopt
   case 'joint'
    phsa = zeros(Ns,1);psba = zeros(Nb-1,1);
    phsb = zeros(Ns,1);psbb = zeros(Nb-1,1);
   case {'independent','none'}
    phsa = zeros(Ns,Nb);psba = [];
    phsb = zeros(Ns,Nb);psbb = [];
  end
  adphs = ad;
  bdphs = bd;
else
  % use provided phase patterns
  switch phsopt
   case 'joint'
    adphs(:,1) = ad(:,1).*exp(1i*phsa);
    bdphs(:,1) = bd(:,1).*exp(1i*phsb);
    for ii = 1:Nb-1
      adphs(:,ii+1) = ad(:,ii+1).*exp(1i*(phsa+psba(ii)));
      bdphs(:,ii+1) = bd(:,ii+1).*exp(1i*(phsb+psbb(ii)));
    end
   case 'independent'
    adphs = ad.*exp(1i*phsa);
    bdphs = bd.*exp(1i*phsb);
  end
end


nn =1; costt = 0;
% algorithm switches/counters and error vector
while ((costt < algp.maintol*cost) || (nn <= algp.mainiters))
        
    cost = costt;
    
%   switch prbp.pulse_func
%       case 1
%           temp = 2 * conj(a) .* b;
%       case 2
%           temp = b.^2;
%       case 3
%           temp = abs(a).^2 - abs(b).^2;
%   end;
%   figure(20+ii); im(abs(embed(temp(:,ii),mask))); axis image; colormap gray; drawnow;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %        update pulses
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [rf,a,b] = rfupdate(rf,brfvec,ntbrf,garea,gam,xx,a,b,adphs,bdphs,sens,f0,lambda,prbp,algp);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    %    update gradients
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [a,b,garea] = gradupdatelt(Brf,rf,complexify(sens),garea,xx,f0,1,prbp.dt,mask,a,b,adphs,bdphs,algp);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculate error
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          
    % Pending Issue: below returns slightly different a & b than above.
    rfmat = reshape(rf,[Nrungs Nc]);
    switch(algp.compmethod)
        case 'mex'
            [a,b] = blochsim_optcont_mex(brfvec,ntbrf,rfmat*prbp.dt*gam,complexify(sens),garea*gam,xx,2*pi*f0*prbp.dt,algp.nthreads);
        otherwise
            error 'Unrecognized compute method'
    end
    
    % Apply shifted phase to desired excitation patterns
    adphs = ad.*exp(1i*angle(a));
    bdphs = bd.*exp(1i*angle(b));
    
    % Calculate Error
    ea = col(a - adphs);
    eb = col(b - bdphs);
    costt = 1/2*real(ea'*ea + eb'*eb) + 1/2*lambda*real(rf'*rf);
           
    fprintf('Iter %d. Flip angle RMSE: %0.4f degrees. RF power: %0.2f. Peak RF: %0.2f.\n',...
        nn,sqrt(mean( abs(2*asin(abs(b(:))) - pi).^2 ))*180/pi,real(rf'*rf),max(abs(rf)));
    
    nn = nn + 1;
    
end

% calculate final phase encode locations from gradient blip areas
kpe = -gambar * flipud(cumsum(flipud(garea),1));

err = cost;
