function [a,b] = garea_to_ab(Af,Bf,garea,xx)

    Ns = size(xx,1); % number of spatial locations
    
    a = ones(Ns,1);
    b = zeros(Ns,1);
    
    Nrungs = size(Af,2);
    
    for jj = 1:Nrungs
      % apply rf rungs
      at = a.*Af(:,jj) - b.*conj(Bf(:,jj));
      bt = a.*Bf(:,jj) + b.*conj(Af(:,jj));

      % apply gradient blips
      zg = exp(1i/2*xx*garea(jj,:)');
      a = at.*zg; b = bt.*conj(zg);
    end
end