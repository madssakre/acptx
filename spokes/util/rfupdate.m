%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RF update function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [rf,a,b] = rfupdate(rf,subrfvec,Ntsubrf,garea,gam,xx,a,b,ad,bd,sens,f0,lambda,prbp,algp)

% get initial cost
cost = cost_eval(a,b,ad,bd,rf,lambda);

% fprintf('RF Update - Itr: 0, Cost: %0.4f.\n',cost);

gRF = 0; nn = 1; costOld = Inf;
while nn <= algp.maxcgiters && (costOld - cost)/cost > algp.cgstopthresh
    
    costOld = cost;
    
    % calculate RF weight derivatives
    gRFold = gRF;
    gRF = rfgradcalc(rf,subrfvec,Ntsubrf,garea,gam,xx,a,b,ad,bd,sens,f0,lambda,prbp,algp);
    
    % get the search direction
    if nn == 1
        dir = -gRF;
    else
        betaRF = max(0,real(gRF'*(gRF-gRFold))/real(gRFold'*gRFold));
        dir = -gRF + betaRF*dir;
    end;
    
    % calculate the step size
    [alpha,a,b] = rfstepcalc(rf,gRF,dir,subrfvec,Ntsubrf,garea,gam,xx,a,b,ad,bd,sens,f0,lambda,prbp,algp);
    
    rf = rf + alpha*dir;
    
    % calculate new cost
    cost = cost_eval(a,b,ad,bd,rf,lambda);
    
%     fprintf('RF Update - Itr: %d, Cost: %0.4f.\n',nn,cost);
    
    nn = nn + 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RF derivative (= gradient) calculation function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function gRF = rfgradcalc(rf,subrfvec,Ntsubrf,garea,gam,xx,a,b,ad,bd,sens,f0,lambda,prbp,algp)

% get search direction (derivatives)
Nrungs = size(garea,1);     % number of spokes/kt-points
Nc = size(sens,2);          % number of tx channels

rfmat = reshape(rf,[Nrungs Nc]);

switch algp.compmethod
    case 'mex'
        gRF = deriv_optcont_mex(subrfvec,Ntsubrf,rfmat*prbp.dt*gam,complexify(sens),...
            garea*gam,xx,f0,algp.nthreads,complexify(a-ad),...
            complexify(b-bd),complexify(a),complexify(b));
    otherwise
        error 'Unrecognized compute method'
end;

% sum across freq bands
gRF = prbp.dt * gam * sum(gRF,2);

% add power penalty derivative
gRF = gRF + lambda*rf;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RF step size calculation function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [tl,a,b] = rfstepcalc(rf,gRF,dir,subrfvec,Ntsubrf,garea,gam,xx,a,b,ad,bd,sens,f0,lambda,prbp,algp)

% calculate current cost
cost = cost_eval(a,b,ad,bd,rf,lambda);

% line search to get step
costt = cost;

al = algp.RFal; bl = algp.RFbl; tl = 1/bl;
while (costt > cost + al*tl*real(gRF'*dir)) && tl > algp.RFmintl
    
    % reduce t
    tl = bl*tl;
    
    % get test point
    rft = rf + tl*dir;
    
    % simulate the test point
    [a,b] = blochsim(rft,subrfvec,Ntsubrf,garea,gam,xx,sens,f0,prbp,algp);
        
    % calculate cost of test point
    costt = cost_eval(a,b,ad,bd,rft,lambda);
        
end

if tl == 1/bl % loop was never entered; return zero step
    tl = 0;
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bloch simulation function
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [a,b] = blochsim(rf,subrfvec,Ntsubrf,garea,gam,xx,sens,f0,prbp,algp)

Ns = size(xx,1);
Nrungs = size(garea,1);
Nc = size(sens,2);

a = zeros(Ns,1);
b = zeros(Ns,1);
rfmat = reshape(rf,[Nrungs Nc]);
switch algp.compmethod
    case 'mex'
            [a,b] = blochsim_optcont_mex(subrfvec,Ntsubrf,rfmat*prbp.dt*gam,...
                complexify(sens),garea*gam,xx,2*pi*f0*prbp.dt,algp.nthreads);
    otherwise
        error 'Unrecognized compute method'
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function cost = cost_eval(a,b,ad,bd,rf,lambda)

ea = col(a - ad);
eb = col(b - bd);
cost = 1/2*((ea'*ea + eb'*eb) + lambda*real(rf'*rf));

end

end