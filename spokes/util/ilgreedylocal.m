function [rf,k,m,A,sterr,beta,phs,psb,mf,favar,compWts,nSVT] = ilgreedylocal(d,sens,xx,ks,...
    kmaxdistance,kinit,Nrungs,f0,fb,rfss,rfssdc,dt,beta,betaadjust,...
    filtertype,phsopt,phs,psb,mask,computemethod,nthreads,Ncred,coilMapping,ncgiters,sensf,xxf,f0f)

% Interleaved greedy and local STA spokes and kT-points pulse design. 2D or 3D supported

% input: 
%   d [Nx x Nb] - target magnitude patterns
%   sens [Nx x Nc] - transmit sensitivities
%   xx [Nx x Ngc] - spatial design grid
%   ks [] - candidate phase encode locations
%   f0 [Nx x 1] - spatial frequency offset/off-resonance map
%   fb [Nb x 1] - frequency offset of each band
%   rfss [Nt x 1] - slice-selective rf pulse for one (non-DC) spoke or point
%   dt [scalar] - RF sampling period (seconds)
%   beta [scalar] - RF Tikhonov penalty parameter (dynamically adjusted during iterations)
%   filtertype [char] - Switch to set spectral characteristic
%   phsopt [scalar] - switch to optimize target excitation phase
%                     1: jointly optimize spatial phase pattern
%                     across spectral bands, with a global shift
%                     between bands (good for fat/water)
%                     2: complete spatial relaxation, each band 
%                     optimized independently
%                     (other): No target phase optimization

% output:
%   rf [Nrungs x Nc] - designed rung weights
%   k [Nrungs x Ngc] - designed phase encodes

Ngc = size(xx,2);
[Nx,Nc] = size(sens); % Nx: # spatial locs, Nc: # tx coils
Nb = length(fb); % number of bands (i.e., fat/water)
if iscell(rfss) % multiple slice-selective subpulses with different lengths
  for ii = 1:length(rfss)
    Nrp(ii) = length(rfss{ii});
  end
else
  Nrp = length(rfss); % number of samples in one rung
  Nrpdc = length(rfssdc);
  dcind = 1; % index of dc pulse
end

compWts = eye(Nc); % initial coil compression matrix for no compression

if isempty(kinit)
  Nrungsinit = 1;
else
  Nrungsinit = size(kinit,1);
end

gambar = 4257;               % gamma/2pi in Hz/T
gam = gambar*2*pi;         % gamma in radians/g

f0 = f0(:);

% get small-tip excitation induced by this pulse
% this is the building block for our system matrix
% only have to account for off-resonance here, since we design at z = 0
if iscell(rfss)
  for ii = 1:length(rfss)
    for jj = 1:Nb
      tr = 0:dt:(Nrp(ii)-1)*dt; % time vector for this rung
      A = 1i*gam*dt*exp(1i*2*pi*(f0+fb(jj))*tr);
      m1rung(:,ii,jj) = A*rfss{ii};
    end
  end
else
  tr = 0:dt:(length(rfss)-1)*dt; % time vector for one rung
  for ii = 1:Nb
    A = 1i*gam*dt*exp(1i*2*pi*(f0+fb(ii))*tr);
    m1rung(:,ii) = A*rfss;
  end
  tr = 0:dt:(length(rfssdc)-1)*dt;
  for ii = 1:Nb
    A = 1i*gam*dt*exp(1i*2*pi*(f0+fb(ii))*tr);
    m1rungdc(:,ii) = A*rfssdc;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% design initial pulse
disp('Designing small-tip pulses');

% initialize desired patterns
% NOTE: Should I initially phase relax across off-res freqs?
if isempty(phs)
  switch phsopt
   case 'joint'
    % joint phase relaxation
    phs = zeros(Nx,1);
    psb = zeros(1,Nb-1);
   case 'independent'
    % independent phase relaxation
    phs = zeros(Nx,Nb);
    psb = [];
   otherwise
    % no phase relaxation
    disp 'No phase relaxation - are you sure you intendend this?'
    phs = [];
    psb = [];
  end
  dwphs = d;
else
  switch(phsopt)
   case 'joint'
    % calculate desired patterns using provided desired phase
    dwphs(:,1) = d(:,1).*exp(1i*phs);
    for ii = 1:Nb-1
      dwphs(:,ii+1) = d(:,ii+1).*exp(1i*(phs+psb(ii)));
    end
   case 'independent'
    % recalculate desired patterns using new desired phase
    dwphs = d.*exp(1i*phs);
  end
end

% stack up spatial locs and sens's across freq bands
xxall = repmat(xx,[Nb 1]);

% precalculate OMP sens+one rung excitation system matrix
if Nrungsinit < Nrungs
  sensall = repmat(sens,[Nb 1]);
  Aomp = pinv(bsxfun(@times,sensall,abs(m1rung(:))));
end

% spoke addition loop
% start with center rung only, then progressively add 2 at a time
% on either side
if isempty(kinit)
  k = zeros(1,Ngc); % get dc spoke to start with
else
  k = kinit;
end

Arungsep = zeros(Nx*Nb,Nrungs);

nSVT = 0;

for Nrungst = Nrungsinit:Nrungs

  cost = [Inf Inf];
  mincost = Inf;
  A = zeros(Nx*Nb,Nc*Nrungst);
  
  % construct design matrix by modifying one rung excitation
  % patterns with appropriate k-space locations, sensitivities, and
  % off-resonance time offset
  for ii = 1:Nrungst
    % blip-induced phase shift
    kphs = xx*k(ii,:)';
    % off res-induced phase shift - account for phase accrual to
    % end of pulse
    for jj = 1:Nb
      if iscell(rfss)
        totphs = exp(1i*2*pi*((f0(:)+fb(jj))*(sum(Nrp(1:ii-1)) - sum(Nrp))*dt+kphs));
        tmp = m1rung(:,ii,jj).*totphs;
      else
        if ii ~= dcind
          totphs = exp(1i*2*pi*((f0+fb(jj))*((ii-1)*Nrp - Nrungst*Nrp)*dt+kphs));
          tmp = m1rung(:,jj).*totphs;
        else
          totphs = exp(1i*2*pi*((f0+fb(jj))*((ii-1)*Nrpdc - Nrungst*Nrpdc)*dt+kphs));
          tmp = m1rungdc(:,jj).*totphs;
        end
      end
      for kk = 1:Nc
        % apply sens, stick it in the design matrix
        A((jj-1)*Nx+1:jj*Nx,(kk-1)*Nrungst+ii) = sens(:,kk).*tmp;
      end
    end
  end
  
  while ((length(cost) == 2) || (cost(end) < 0.9999*cost(end-1)) ...
        || (length(cost)-2 <= 100))
          
    % update RF 
    if Nrungst <= Ncred % if not enough subpulses to do compression
        
        % design subpulse weights using pseudoinverse
        if Nrungst > 1
            rf = (A'*A+beta*speye(Nrungst*Nc))\(A'*dwphs(:));
        else
            rf = A\dwphs(:);
        end
    
    else
        
        % take a couple CG iterations
        [xS,~] = qpwls_pcg(rf,A,1,dwphs,0,sqrt(beta),1,ncgiters,ones(numel(rf),1));
        rf = xS(:,end);
        
        rfw = reshape(rf,[Nrungst Nc]);
        % SVT Compression
        if isempty(coilMapping)
            [u,s,v] = svd(rfw.','econ');
            s(Ncred+1:end,Ncred+1:end) = 0; % Hard thresholding
            rfw = (u*(s*v')).';rf = rfw(:);
            compWts = u(:,1:Ncred); % compression weights
        else
            rfwt = rfw;
            compWts = [];
            for ii = 1:size(coilMapping,2)
                [u,s,v] = svd(rfw(:,coilMapping(:,ii)).','econ');
                s((Ncred+1):end,(Ncred+1):end) = 0; % Hard thresholding
                rfwt(:,coilMapping(:,ii)) = (u*(s*v')).';
                compWts(:,ii) = u(:,1:Ncred);
            end
            rf = rfwt(:);
        end
        nSVT = nSVT + 1;
        
    end
        
    % get excitation patterns
    m = reshape(A * rf,[Nx Nb]);
    
    % optimize desired phase pattern
    switch(phsopt)
     case 'joint'
      % jointly optimize spatial phase pattern,
      % with a global shift between bands
      [phs,psb] = phase_update(d,m,phs,psb,5);
      % recalculate desired patterns using new desired phase
      dwphs(:,1) = d(:,1).*exp(1i*phs);
      for ii = 1:Nb-1
        dwphs(:,ii+1) = d(:,ii+1).*exp(1i*(phs+psb(ii)));
      end
     case 'independent'
      % completely independent phase relaxation across bands
      phs = angle(m);
      % recalculate desired patterns using new desired phase
      dwphs = d.*exp(1i*phs);
    end
            
    % optimize phase encodes
    if Nrungst > 1
      % get A*b, separated for each rung
      for ii = 1:Nrungst
        Arungsep(:,ii) = A(:,ii:Nrungst:end)*rf(ii:Nrungst:end);
      end
      if isreal(dwphs)
        dwphs = complex(dwphs);
      end
      % get locally-optimal gradient blip area changes
      switch(computemethod)
       case 'mex'
        if size(xxall,2) == 2
          [vx,Sx,vy,Sy,vz,Sz] = kGradHess(Arungsep(:,1:Nrungst),dwphs(:),[xxall zeros(size(xxall(:,1)))],nthreads);
        else
          [vx,Sx,vy,Sy,vz,Sz] = kGradHess(Arungsep(:,1:Nrungst),dwphs(:),xxall,nthreads);
        end
       case 'gpu'
        [vx,Sx,vy,Sy,vz,Sz] = kGradHess_cuda(Arungsep(:,1:Nrungst),dwphs(:),xxall,Nrungs);
       otherwise
        error 'Unrecognized compute method'
      end
      dk = [];
      dk(:,1) = Sx\vx;
      dk(:,2) = Sy\vy;
      if Ngc == 3
        dk(:,3) = Sz\vz;
      end
      %dk = 0*dk;
      k = k + dk;
      %k(3) = k(1);k(2) = 0;
      % add phase to A and update m
      for ii = 1:Nrungst
        A(:,ii:Nrungst:end) = bsxfun(@times,A(:,ii:Nrungst:end),exp(1i*2*pi*(xxall*dk(ii,:).')));
      end
      m = reshape(A*rf,[Nx Nb]);
    end
            
    % evaluate error
    cost(end+1) = 1/2*norm(m(:)-dwphs(:))^2;
    if Nrungst > 1
      RFreg = 1/2*beta*real(rf'*rf);
    else
      RFreg = 0;
    end
    if rem(length(cost)-2,10) == 0
      disp(sprintf('Iter %d, %d points. Mean flip angle: %0.4f degrees. Flip angle std: %0.4f degrees. Peak RF: %0.2f.',...
                   length(cost)-2,Nrungst,mean(abs(m(abs(d) > 0)))*180/pi,std(abs(m(abs(d) > 0)))*180/pi,max(abs(rf))));
    end
    
    favar = var(abs(m(abs(d) > 0)))*180/pi;
    % check to make sure that RF regularization isn't too big/too small
    if betaadjust && (rem(length(cost)-2,50) == 0) && ...
          ((RFreg > 1.25*cost(end)) || (RFreg < 0.75*cost(end))) ...
          && (Nrungst > 1) && (cost(end) < 1.25*mincost)
      disp('Adjusting RF penalty');
      beta = cost(end)/RFreg*beta;
      mincost = min([cost mincost]);
      cost = [inf inf];
    else      
      cost(end) = cost(end) + RFreg;
    end
  end
    
  % add a spoke
  if Nrungst < Nrungs

    odds = rem(Nrungst,2);
    
    % get excitation basis matrix for this spoke location (without
    % k-space loc phase)
    m1rungphs = [];
    for jj = 1:Nb
      if (odds && strcmp(filtertype,'Lin phase')) || strcmp(filtertype,'Min phase') % add phase for first rung in pulse
        offresphs = exp(1i*2*pi*(f0+fb(jj))*(-(Nrungst+1)*Nrp)*dt);
      else % add phase for last rung in pulse
        offresphs = exp(1i*2*pi*(f0+fb(jj))*(-Nrp)*dt);
      end
      m1rungphs(:,jj) = m1rung(:,jj).*offresphs;
    end
    m1rungphs = m1rungphs(:); % stack frequency bands

    if (~odds & strcmp(filtertype,'Lin phase')) | strcmp(filtertype,'Max phase') % if adding a rung on the end of the pulse
      % advance m phase to account for later observation time
      for jj = 1:Nb
        offresphs = exp(1i*2*pi*(f0+fb(jj))*(-Nrp)*dt);
        m(:,jj) = m(:,jj).*offresphs;
      end
      % advance desired phase to account for later observation time
      f0phsadd = -2*pi*f0*Nrp*dt; % additional phase due to main
                                     % field offsets
      fbphsadd = -2*pi*fb*Nrp*dt; % additional phase due to
                                  % frequency band offsets
      switch(phsopt)
       case 'joint'
        phs(:,1) = phs(:,1) + f0phsadd + fbphsadd(1);
        % joint optimization of spatial phase pattern,
        % with a global shift between bands
        dwphs(:,1) = d(:,1).*exp(1i*phs);
        for ii = 1:Nb-1
          psb(ii) = psb(ii) + fbphsadd(ii+1) - fbphsadd(1);
          dwphs(:,ii+1) = d(:,ii+1).*exp(1i*(phs+psb(ii)));
        end
       case 'independent'
        phs(:,1) = phs(:,1) + f0phsadd + fbphsadd(1);
        % completely independent phase relaxation across bands
        dwphs(:,1) = d(:,1).*exp(1i*phs(:,1));
        for ii = 1:Nb-1
          phs(:,ii+1) = phs(:,ii+1) + f0phsadd + fbphsadd(ii+1);
          dwphs(:,ii+1) = d(:,ii+1).*exp(1i*phs(:,ii+1));
        end
      end
    end

    disp('Adding a kt-point using orthogonal matching pursuit');
    
    % calculate magnitude of pulses for each k-loc
    % try basing selection on actual error after adding this spoke?
    % can i calculate terms for solo for the rest of the spokes
    % before looking at new candidates, to reduce cost?
    res = dwphs(:) - m(:); % initialize residual to current error
    res = exp(-1i*angle(m1rungphs)).*res;
    rfnorm = [];    
    if (odds & strcmp(filtertype,'Lin phase')) | strcmp(filtertype,'Min phase')
      % spoke will be added to beginning of pulse
      kclosest = k(1,:);
    else 
      % add spoke to end of pulse
      kclosest = k(end,:);
    end
    parfor jj = 1:length(ks)
      if ~any(abs(ks(jj,:) - kclosest) > kmaxdistance) % if this point isnt too far
        rfnorm(jj) = norm(Aomp*(exp(-1i*2*pi*(xxall*ks(jj,:).')).*res));
      else
        rfnorm(jj) = 0;
      end
    end
    % add the k-loc with max norm to the list
    [~,maxi] = max(rfnorm);
                    
    rf = reshape(rf,[length(rf)/Nc Nc]);
    if (odds & strcmp(filtertype,'Lin phase')) | strcmp(filtertype,'Min phase')
      % add spoke to beginning of pulse
      k = [ks(maxi,:); k];
      rf = [zeros(1,Nc); rf];
      dcind = dcind + 1;
    else 
      % add spoke to end of pulse
      k = [k; ks(maxi,:)];
      rf = [rf; zeros(1,Nc)];
    end
    rf = rf(:);

  end
end

if nargout > 4
  sterr(1) = cost(end); % return metric for DOE
  sterr(2) = cost(end)-RFreg; % second entry is excitation
                                % error only 
end

if Nrungs > 1 & betaadjust
  % normalize again for design on smaller grads
  beta = (cost(end)-RFreg)/RFreg*beta;
end

if nargin == 26 && nargout == 11 % user provided full xx, sens to evaluate mf
  % get small-tip excitation induced by this pulse
  % this is the building block for our system matrix
  % only have to account for off-resonance here, since we design at z = 0
  m1rung = [];m1rungdc = [];
  if iscell(rfss)
    for ii = 1:length(rfss)
      for jj = 1:Nb
        tr = 0:dt:(Nrp(ii)-1)*dt; % time vector for this rung
        A = 1i*gam*dt*exp(1i*2*pi*(f0f(:)+fb(jj))*tr);
        m1rung(:,ii,jj) = A*rfss{ii};
      end
    end
  else
    tr = 0:dt:(length(rfss)-1)*dt; % time vector for one rung
    for ii = 1:Nb
      A = 1i*gam*dt*exp(1i*2*pi*(f0f(:)+fb(ii))*tr);
      m1rung(:,ii) = A*rfss;
    end
    tr = 0:dt:(length(rfssdc)-1)*dt;
    for ii = 1:Nb
      A = 1i*gam*dt*exp(1i*2*pi*(f0f(:)+fb(ii))*tr);
      m1rungdc(:,ii) = A*rfssdc;
    end
  end

  [Nx,Nc] = size(sensf); % Nx: # spatial locs, Nc: # tx coils
  A = zeros(Nx*Nb,Nc*Nrungst);
  
  % construct design matrix by modifying one rung excitation
  % patterns with appropriate k-space locations, sensitivities, and
  % off-resonance time offset
  for ii = 1:Nrungst
    % blip-induced phase shift
    kphs = xxf*k(ii,:)';
    % off res-induced phase shift - account for phase accrual to
    % end of pulse
    for jj = 1:Nb
      if iscell(rfss)
        totphs = exp(1i*2*pi*((f0f(:)+fb(jj))*(sum(Nrp(1:ii-1)) - sum(Nrp))*dt+kphs));
        tmp = m1rung(:,ii,jj).*totphs;
      else
        if ii ~= dcind
          totphs = exp(1i*2*pi*((f0f(:)+fb(jj))*((ii-1)*Nrp - Nrungst*Nrp)*dt+kphs));
          tmp = m1rung(:,jj).*totphs;
        else
          totphs = exp(1i*2*pi*((f0f(:)+fb(jj))*((ii-1)*Nrpdc - Nrungst*Nrpdc)*dt+kphs));
          tmp = m1rungdc(:,jj).*totphs;
        end
      end
      for kk = 1:Nc
        % apply sens, stick it in the design matrix
        A((jj-1)*Nx+1:jj*Nx,(kk-1)*Nrungst+ii) = sensf(:,kk).*tmp;
      end
    end
  end

  mf = A*rf;
    
else
  mf = [];
end
