function [a,b,agu,bgu] = blochsim_optcont_gut(Brf,rfw,sens,garea,xx,omdt,nthreads,computemethod)

% inputs
% Brf     {Nrungs} - basis slice-selective waveforms for each rung
% rfw     [Nrungs Nc] - rf weights for each rung and each coil
% sens    [Ns Nc] - tx sensitivities for each coil
% omdt    [Ns 1]  - off-resonance map
% getgu   {scalar} - switch to calculate gradient update terms

agu = [];bgu = [];

[Ns,~] = size(xx); % Ns: # spatial locs. Nd: # of dimensions
Nrungs = length(Brf);

a = ones(Ns,1);
b = zeros(Ns,1);

% off-resonance
zom = exp(1i/2*omdt);

usemex = 1;
for jj = 1:Nrungs

  % store state prior to this rung, for gradient update term calc
  aold = a;bold = b;
  
  %sensrf = sens*rfw(jj,:).'; % shim for this rung

  if ~usemex
    for ii = 1:length(Brf{jj})
      
      b1 = Brf{jj}(ii)*sensrf;
      
      % apply rf
      C = cos(abs(b1)/2);
      S = 1i*exp(1i*angle(b1)).*sin(abs(b1)/2);
      at = a.*C - b.*conj(S);
      bt = a.*S + b.*C;
      
      a = at;b = bt;
      
      % apply off-resonance
      a = a.*zom;
      b = b.*conj(zom);
      
    end
  else
    if isreal(a)
      a = complex(a);
    end
    if isreal(b)
      b = complex(b);
    end
    switch(computemethod)
     case 'mex'
      % concatenate pulses in Brf
      [arf,brf] = blochsim_optcont_mex(Brf{jj}(:),length(Brf{jj}(:)),rfw(jj,:),complexify(sens),0*garea(jj,:),xx,omdt,nthreads);
     case 'gpu'
      [arf,brf] = blochsim_optcont_cuda(complexify(sens),omdt,rfw(jj,:),Brf{jj}(:),xx,0*garea(jj,:),length(Brf{jj}(:)));
     otherwise
      error 'Unrecognized compute method'
    end
    at = arf.*a - conj(brf).*b;
    bt = brf.*a + conj(arf).*b;
    a = at;b = bt;
  end

  % apply gradient blips
  zg = exp(1i/2*xx*garea(jj,:)');
  a = a.*zg;b = b.*conj(zg);
  
  if ~isempty(agu)
    % invert a, b to get rotations due to this rung + gradient
    arung = a.*conj(aold) + conj(b).*bold;
    brung = b.*conj(aold) - conj(a).*bold;
    agut = bsxfun(@times,arung,agu);
    bgut = bsxfun(@times,brung,agu);
    agut = [agut bsxfun(@times,-conj(brung),bgu)];
    bgut = [bgut bsxfun(@times,conj(arung),bgu)];
    agu = agut;bgu = bgut;
  else
    % first rung. just store current state
    agu = a;bgu = b;
  end
  
end

