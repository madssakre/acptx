function [rf,kpe,a,b,Brf,beta,avgflip,phsa,psba,phsb,psbb,cost,compWts,nSVT] = lt_oc(ad,bd,sens,xx,kinit,f0,fb,rfss,z_tbw,d1,d2,dt,gpos,gfly,rampsamp,nramp,beta,betath,rf,phsopt,phsa,psba,phsb,psbb,mask,computemethod,nthreads,ltgradopt,Ncred,coilMapping)

disp('Designing large-tip pulses.')

gambar = 4257;             % gamma/2pi in Hz/T
gam = gambar*2*pi;         % gamma in radians/g

% calculate area of phase encodes from kpe
garea = diff([kinit;zeros(1,size(kinit,2))],1)/gambar;

Nrungs = size(garea,1);

Nc = size(sens,2); % number of tx channels
Ns = size(xx,1); % number of spatial locations
Nb = length(fb); % number of frequency bands

xxmb = repmat(xx,[Nb 1]);

compWts = eye(Nc); % compression weights

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RF basis for lt design
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if z_tbw > 0
  [Brf,avgflip,rf] = genRFbasis(Nrungs,dt,rfss,sens,rf,d1,d2, ...
                                z_tbw,rampsamp,gpos,nramp,gfly);
  % convert Brf to vector for mex blochsim
  brfvec = [];
  for ii = 1:length(Brf)
    brfvec = [brfvec;Brf{ii}(:)];
    ntbrf(ii) = length(Brf{ii});
  end
else % kT-points - the subpulses won't change
  % convert Brf to vector for mex blochsim
  brfvec = [];
  for ii = 1:length(rfss)
    brfvec = [brfvec;rfss{ii}];
    ntbrf(ii) = length(rfss{ii});
  end
  avgflip = [];
  Brf = rfss;
end

% get relationship between phase encodes and poly terms
[Ag,Bg,Ans,Bns] = get_ab_to_g(Nrungs);

% target phase variables and phase-modified patterns
if isempty(phsa)
  switch phsopt
   case 'joint'
    phsa = zeros(Ns,1);psba = zeros(Nb-1,1);
    phsb = zeros(Ns,1);psbb = zeros(Nb-1,1);
   case {'independent','none'}
    phsa = zeros(Ns,Nb);psba = [];
    phsb = zeros(Ns,Nb);psbb = [];
  end
  adphs = ad;
  bdphs = bd;
else
  % use provided phase patterns
  switch phsopt
   case 'joint'
    adphs(:,1) = ad(:,1).*exp(1i*phsa);
    bdphs(:,1) = bd(:,1).*exp(1i*phsb);
    for ii = 1:Nb-1
      adphs(:,ii+1) = ad(:,ii+1).*exp(1i*(phsa+psba(ii)));
      bdphs(:,ii+1) = bd(:,ii+1).*exp(1i*(phsb+psbb(ii)));
    end
   case 'independent'
    adphs = ad.*exp(1i*phsa);
    bdphs = bd.*exp(1i*phsb);
  end
end

% algorithm switches/counters and error vector
cost = [Inf Inf];
mincost = Inf;
phsparama = [];
phsparamb = [];
nSVT = 0;

while ((cost(end) < 0.9999*cost(end-1))) || (length(cost)-2 <= 100)
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % Build new object with latest gradients, RF basis
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % Bloch sim with new rf basis
  a = zeros(Ns,Nb);
  b = zeros(Ns,Nb);
  rfmat = reshape(rf,[Nrungs Nc]);
  switch(computemethod)
   case 'dotm'
    for ii = 1:Nb
      [a(:,ii),b(:,ii)] = blochsim_optcont(Brf,rfmat*dt*gam,sens,garea*gam,xx,2*pi*(f0+fb(ii))*dt);
    end
   case 'mex'
    for ii = 1:Nb
      [a(:,ii),b(:,ii)] = blochsim_optcont_mex(brfvec,ntbrf,rfmat*dt*gam,complexify(sens),garea*gam,xx,2*pi*(f0+fb(ii))*dt,nthreads);
    end
   case 'gpu'
    for ii = 1:Nb
      [a(:,ii),b(:,ii)] = blochsim_optcont_cuda(sens,2*pi*(f0+fb(ii))*dt,rfmat*dt*gam,brfvec,xx,garea*gam,ntbrf);
    end
   otherwise
    error 'Unrecognized compute method'
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % update target excitation phase
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  switch phsopt
   case 'joint'
    if any(abs(ad) > 0) % ad = 0 if 180 so target phase doesnt matter
      [phsa,psba] = phase_update(ad,a,phsa,psba,5);
    end
    [phsb,psbb] = phase_update(bd,b,phsb,psbb,5);
    % apply shifted phase to desired excitation patterns
    adphs(:,1) = ad(:,1).*exp(1i*phsa);
    bdphs(:,1) = bd(:,1).*exp(1i*phsb);
    for ii = 1:Nb-1
      adphs(:,ii+1) = ad(:,ii+1).*exp(1i*(phsa+psba(ii)));
      bdphs(:,ii+1) = bd(:,ii+1).*exp(1i*(phsb+psbb(ii)));
    end
   case 'independent'
    if any(abs(ad) > 0)
      phsa = angle(a);
    end
    phsb = angle(b);
    % apply shifted phase to desired excitation patterns
    adphs = ad.*exp(1i*phsa);
    bdphs = bd.*exp(1i*phsb);
   case 'none'
    if length(cost)-2 == 0
      if any(abs(ad) > 0)
        phsa = angle(a);
      end
      phsb = angle(b);
      % apply shifted phase to desired excitation patterns
      adphs = ad.*exp(1i*phsa);
      bdphs = bd.*exp(1i*phsb);
    end
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % calculate error
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  ea = col(a - adphs);
  eb = col(b - bdphs);
  cost(end+1) = 1/2*real(ea'*ea + eb'*eb);
  RFreg = 1/2*beta*real(rf'*rf);
  
  fprintf('Iter %d. Mean flip angle: %0.4f degrees. Flip angle variance: %0.4f degrees. Peak RF: %0.2f.\n',...
      length(cost)-2,mean(abs(2*asin(abs(b(:)))))*180/pi,var(abs(2*asin(abs(b(:)))))*180/pi,max(abs(rf)));
  
  % check to make sure that RF regularization is big enough
  if (rem(length(cost)-2,10) == 0) && ...
        ((RFreg > 1.25*cost(end)) || (RFreg < 0.75*cost(end))) ...
        && (cost(end) < 1.25*mincost)
    disp('Adjusting RF penalty.');
    beta = cost(end)/RFreg*beta;
    mincost = min([cost mincost]);
    cost = [inf cost(end)+1/2*beta*real(rf'*rf)];
  else
    cost(end) = cost(end) + RFreg;
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % update pulses
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  % get search direction (derivatives)
  switch(computemethod)
   case 'mex'
    for ii = 1:Nb
      drf(:,ii) = deriv_optcont_mex(brfvec,ntbrf,rfmat*dt*gam,complexify(sens),garea*gam,xx,2*pi*(f0+fb(ii))*dt,nthreads,(a(:,ii)-adphs(:,ii)),(b(:,ii)-bdphs(:,ii)),a(:,ii),b(:,ii));
    end   
   case 'gpu'
    for ii = 1:Nb
      drf(:,ii) = deriv_optcont_cuda(sens,2*pi*(f0+fb(ii))*dt,rfmat*dt*gam,brfvec,xx,garea*gam,ntbrf,(a(:,ii)-adphs(:,ii)),(b(:,ii)-bdphs(:,ii)),a(:,ii),b(:,ii));
    end
   otherwise
    error 'Unrecognized compute method'
  end
  
  % sum across freq bands
  drf = dt*gam*sum(drf,2);
  % add power penalty derivative
  drf = drf + beta*rf;
  
  % line search to get step
  costt = cost(end);
  tl = 1;
  al = 0.3; bl = 0.3;
  while (costt > cost(end) - al*tl*real(drf'*drf))
    
    % get test point
    rft = rf - tl*drf;
    
    % simulate
    at = zeros(Ns,Nb);
    bt = zeros(Ns,Nb);
    rfmat = reshape(rft,[Nrungs Nc]);
    switch(computemethod)
     case 'dotm'
      for ii = 1:Nb
        [at(:,ii),bt(:,ii)] = blochsim_optcont(Brf,rfmat*dt*gam,sens,garea*gam,xx,2*pi*(f0+fb(ii))*dt);
      end
     case 'mex'
      for ii = 1:Nb
        [at(:,ii),bt(:,ii)] = blochsim_optcont_mex(brfvec,ntbrf,rfmat*dt*gam,complexify(sens),garea*gam,xx,2*pi*(f0+fb(ii))*dt,nthreads);
      end
     case 'gpu'
      for ii = 1:Nb
        [at(:,ii),bt(:,ii)] = blochsim_optcont_cuda(sens,2*pi*(f0+fb(ii))*dt,rfmat*dt*gam,brfvec,xx,garea*gam,ntbrf);
      end
     otherwise
      error 'Unrecognized compute method'
    end
    
    % calculate cost
    ea = col(at - adphs);
    eb = col(bt - bdphs);
    costt = 1/2*real(ea'*ea + eb'*eb) + 1/2*beta*real(rft'*rft);
    
    % reduce t
    tl = bl*tl;
    
  end
  rf = rft;
  a = at;b = bt;
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % compress the RF, re-simulate
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if Nrungs > Ncred
      
      rfw = reshape(rf,[Nrungs Nc]);
      % SVT Compression
      if isempty(coilMapping)
          [u,s,v] = svd(rfw.','econ');
          s(Ncred+1:end,Ncred+1:end) = 0; % Hard thresholding
          rfw = (u*(s*v')).';rf = rfw(:);
          compWts = u(:,1:Ncred); % compression weights
      else
          rfwt = rfw;
          compWts = [];
          for ii = 1:size(coilMapping,2)
              [u,s,v] = svd(rfw(:,coilMapping(:,ii)).','econ');
              s((Ncred+1):end,(Ncred+1):end) = 0; % Hard thresholding
              rfwt(:,coilMapping(:,ii)) = (u*(s*v')).';
              compWts(:,ii) = u(:,1:Ncred);
          end
          rf = rfwt(:);
      end
      nSVT = nSVT + 1;
      
      % simulate
      a = zeros(Ns,Nb);
      b = zeros(Ns,Nb);
      rfmat = reshape(rf,[Nrungs Nc]);
      switch(computemethod)
          case 'dotm'
              for ii = 1:Nb
                  [a(:,ii),b(:,ii)] = blochsim_optcont(Brf,rfmat*dt*gam,sens,garea*gam,xx,2*pi*(f0+fb(ii))*dt);
              end
          case 'mex'
              for ii = 1:Nb
                  [a(:,ii),b(:,ii)] = blochsim_optcont_mex(brfvec,ntbrf,rfmat*dt*gam,complexify(sens),garea*gam,xx,2*pi*(f0+fb(ii))*dt,nthreads);
              end
          case 'gpu'
              for ii = 1:Nb
                  [a(:,ii),b(:,ii)] = blochsim_optcont_cuda(sens,2*pi*(f0+fb(ii))*dt,rfmat*dt*gam,brfvec,xx,garea*gam,ntbrf);
              end
          otherwise
              error 'Unrecognized compute method'
      end
      
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%
  % update gradients
  %%%%%%%%%%%%%%%%%%%%%%%%%%%
  if ltgradopt
    % new idea: get all RF alpha and beta terms during derivative calc,
    % then do a line search to get step size, using the precomputed RF alpha and beta terms
    
    % Bloch sim to get grad opt terms
    rfmat = reshape(rf,[Nrungs Nc]);
    A = [];B = [];
    for ii = 1:Nb
      [~,~,agut,bgut] = blochsim_optcont_gut(Brf,rfmat*dt*gam,complexify(sens),garea*gam,xx,2*pi*(f0+fb(ii))*dt,nthreads,computemethod);
      A = [A;agut];B = [B;bgut];
    end

    %dga = 0;dgb = 0;
    %for ii = 1:Nb
    %  [tmp1,tmp2] = kGradlt(Brf,rfmat*dt*gam,complexify(sens),garea*gam,xx,2*pi*(f0+fb(ii))*dt,nthreads);
    %  dga = dga+tmp1;dgb = dgb+tmp2;
    %end
    %ea = col(At - adphs);
    %eb = col(Bt - bdphs);
    %keyboard
    
    % get total A, B
    At = sum(A,2);Bt = sum(B,2);
    At = reshape(At,[Ns Nb]);
    Bt = reshape(Bt,[Ns Nb]);
    
    % calculate error
    ea = col(At - adphs);
    eb = col(Bt - bdphs);
    costgupdate = 1/2*real(ea'*ea + eb'*eb);
    
    Aorig = A;Borig = B;
    
    % calculate step using optimization transfer
    dsumgdt = grad_update_lt_mb(A,B,adphs(:),bdphs(:),xxmb,...
                                Ag,Bg,Ans,Bns,betath,nthreads);

    % apply differential phase shifts to A, B
    parfor ii = 1:size(A,2)
      A(:,ii) = Aorig(:,ii).*exp(1i*gam/2*xxmb*(Ag{ii}*dsumgdt)');
      B(:,ii) = Borig(:,ii).*exp(1i*gam/2*xxmb*(Bg{ii}*dsumgdt)');
    end
  
    % get total A, B
    At = sum(A,2);Bt = sum(B,2);
    At = reshape(At,[Ns Nb]);
    Bt = reshape(Bt,[Ns Nb]);
    
    % calculate error
    ea = col(At - adphs);
    eb = col(Bt - bdphs);
    costgupdate = [costgupdate 1/2*real(ea'*ea + eb'*eb)];
    
    if costgupdate(end) > costgupdate(end-1)
      dsumgdt = 0;
      disp('WARNING: NON-MONOTONIC GRAD UPDATE');
    end
    
    % add on gradient update
    garea = garea + dsumgdt;
  end

  if z_tbw > 0 % if not kT-points
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    % update the RF basis
    %%%%%%%%%%%%%%%%%%%%%%%%%%%
    [Brf,avgflip,rf] = genRFbasis(Nrungs,dt,Brf,sens,rf,d1,d2,z_tbw,rampsamp,gpos,nramp,gfly);
    % convert Brf to vector for mex blochsim
    brfvec = [];
    for ii = 1:length(Brf)
      brfvec = [brfvec;Brf{ii}(:)];
      ntbrf(ii) = length(Brf{ii});
    end
  
  end

end

% calculate final phase encode locations from gradient blip areas
kpe = -gambar * flipud(cumsum(flipud(garea),1));

% balance RF regularization
beta = (cost(end)-RFreg)/RFreg*beta;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% function to generate RF basis using SLR 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Brf,avgflip,rf] = genRFbasis(Nrungs,dt,Brf,sens,rf,d1,d2,z_tbw,rampsamp,gpos,nramp,gfly)

rf = reshape(rf,[Nrungs length(rf)/Nrungs]);

Ns = size(sens,1);

gambar = 4257;             % gamma/2pi in Hz/T
gam = gambar*2*pi;         % gamma in radians/g

% determine avg flip for each rung
avgflip = [];
for ii = 1:Nrungs
  if ~iscell(Brf)
    sssum = abs(sum(Brf));
  else
    sssum = abs(sum(Brf{ii}));
  end
  avgflip(ii) = gam*dt*sssum*sum(abs(sens*rf(ii,:).'));
end
avgflip = avgflip/Ns;

% design slr pulses to hit these flips at isocenter
for ii = 1:Nrungs
  bsf = sin(avgflip(ii)/2);
  np = 128; 
  rfslr = slr(np,z_tbw,d1,d2,bsf);
  %bslr = bsf*dzls(np,z_tbw,d1,d2);
  %rfslr = real(col(b2rf(bslr)));
  if rampsamp
    % verse the pulse onto the actual trajectory
    rfslr = [verse(gpos{ii},rfslr);zeros(length(gfly{ii}),1)];
  else
    % no verse-ing, just add zeros to account for attack and decay
    rfslr = [zeros(nramp(ii),1);verse(gpos{ii}(nramp(ii)+1:end-nramp(ii)),rfslr);zeros(nramp(ii),1);zeros(length(gfly{ii}),1)];
  end
  Brfnew{ii} = rfslr(:)/max(abs(rfslr));
  % adjust rf weights to achieve same flip in slice center with the
  % new pulse
  if ~iscell(Brf)
    rf(ii,:) = rf(ii,:)*sum(Brf)/sum(Brfnew{ii});
  else
    rf(ii,:) = rf(ii,:)*sum(Brf{ii})/sum(Brfnew{ii});
  end

end

Brf = Brfnew;
rf = rf(:);
