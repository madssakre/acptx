function [Ag,Bg,Anb,Bnb] = get_ab_to_g(Ng)

% given the number of gradient encodes, get a matrix relating the
% gradient areas to the areas of the final complex exponentials

% do first one manually
Ag = {[1]}; 
Bg = {[-1]};
% number of sines in terms after first RF segment
Anb = 0;
Bnb = 1;

% apply gradient/rf pairs
for ii = 2:Ng
  % apply RF
  Agtmp = Ag;Bgtmp = Bg;
  Ag = {Agtmp{:},Bgtmp{:}};
  Bg = {Agtmp{:},Bgtmp{:}};
  % calculate number of sine terms
  Anbtmp = Anb;Bnbtmp = Bnb;
  Anb = [Anbtmp Bnbtmp+1];
  Bnb = [Anbtmp+1 Bnbtmp];
  % apply gradient
  for jj = 1:length(Ag)
    Ag{jj} = [Ag{jj} 1];
    Bg{jj} = [Bg{jj} -1];
  end
end
