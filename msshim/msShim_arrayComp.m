function [rfComp,mComp,errComp,powComp,nSVTSv,compWtsSv] = msShim_arrayComp(maps,prbp,algp)

% Script to design a multi-slice RF shimming compressed pulse (32Ch -> 2Ch)
% Copyright Zhipeng Cao and Will Grissom, Vanderbilt University, 2015

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Problem parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
beta = algp.beta; % 0    % RF regularization parameter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build system matrix for each slice
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[dimxy(1),dimxy(2),Nsl,Nc] = size(maps.b1); % number of physical coils (Ncoils)

for idx = 1:Nsl

    % reorg b1 maps into tall skinny matrix (row: space, col: coil)
    tmp = permute(squeeze(maps.b1(:,:,idx,:)),[3 1 2]);
    tmp = tmp(:,:).';
    % mask them
    tmp = tmp(logical(maps.mask(:,:,idx)),:);
    % remove coil 1 phase
    A{idx} = bsxfun(@times,exp(-1i*angle(tmp(:,1))),tmp);
    
    % init target phase pattern
    if ~isfield(maps,'phsinit')
      dphs{idx} = zeros(sum(col(maps.mask(:,:,idx))),1);
    else
      tmp = maps.phsinit(:,:,idx);
      dphs{idx} = tmp(col(maps.mask(:,:,idx)));
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do compressed design for increasing # of channels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
rfw = zeros(Nc,Nsl); % placing initializer here means that each Nchan initializes Nchan+1
errSv = [];
rfwSv = [];
nSVTSv = [];
errComp = [];
powComp = [];
compWtsSv = {};
rfComp = {};
mComp = {};
for Nccompt = prbp.Ncred
    
    fprintf('%d channel(s)\n',Nccompt);
    if Nccompt < min(Nc,Nsl)
      flag = 1; % to get loop started
      err = Inf; % initial error
      itr = 1; % iteration counter
      nSVT = 0;
      while flag
        
        % calculate optimal shim for each slice with current target phase
        for idx = 1:Nsl
          % take a couple CG iterations
          [xS,info] = qpwls_pcg(rfw(:,idx),[A{idx};sqrt(beta)*eye(size(rfw(:,idx),1))],1,...
                                [exp(1i*dphs{idx});0*rfw(:,idx)],0,0,1,algp.ncgiters,ones(size(rfw,1),1));
          rfw(:,idx) = xS(:,end);
        end
        
        % threshold the SVD of the weights matrix
        [u,s,v] = svd(rfw,'econ');
        
        % Hard truncation
        s((Nccompt+1):end,(Nccompt+1):end) = 0;
        rfw = u*(s*v');
        compWts = u(:,1:Nccompt); 
        
        nSVT = nSVT + 1;
        
        % set the target phase equal to the phase of the resulting excitation
        % patterns
        for idx = 1:Nsl
            dphs{idx} = angle(A{idx}*rfw(:,idx));
        end
        
        % calculate and report the error of this feasible point
        itr = itr + 1;
        err(itr) = 0;
        for idx = 1:Nsl
            err(itr) = err(itr) + 1/2*norm(A{idx}*rfw(:,idx) - exp(1i*dphs{idx}))^2;
        end
        err(itr) = err(itr) + beta/2*norm(rfw)^2;
        if rem(itr,10) == 0;fprintf('%d Channel(s). Iteration %d. Cost %f.\n',Nccompt,itr,err(itr));end
        
        if err(itr) > algp.tol*err(itr-1)
            flag = 0;
        end
        
      end
      fprintf('Final %d-channel cost: %f.\n',Nccompt,err(itr));
      rfwSv(:,:,Nccompt) = rfw;
      errSv(end+1) = err(end);
      compWtsSv{end+1} = compWts;
      R = beta*eye(Nccompt);
      nSVTSv(end+1) = nSVT;
    else
      compWtsSv{end+1} = eye(Nc);
      R = beta*eye(Nc);
    end
    
    % re-design the weights using the final B1 maps, no RF regularization
    disp('Redesigning the weights with the compressed B1 maps');
    rfComp{end+1} = [];
    mComp{end+1} = [];
    for idx = 1:Nsl
        
        % combine B1 maps using final compression weights 
        Acomp = A{idx}*compWtsSv{end};
        
        % precalculate regularized pseudoinverse
        Ainv = (Acomp'*Acomp + R)\Acomp'; 
        
        flag = 1; % to get loop started
        err = Inf; % initial error
        itr = 1; % iteration counter
        dphscomp = dphs{idx};
        while flag
            
            % calculate RF weights
            rfComp{end}(:,idx) = Ainv*exp(1i*dphscomp);
            
            % calculate the excitation pattern
            m = Acomp*rfComp{end}(:,idx);
            
            % update the target phase pattern
            dphscomp = angle(m);
            
            % calculate error
            itr = itr + 1;    
            err(itr) = 1/2*norm(m-exp(1i*dphscomp))^2;
            if err(itr) > algp.tol*err(itr-1)
                flag = 0;
            end
            
            if rem(itr,10) == 0;fprintf('%d Channel(s). Iteration %d. Cost %f.\n',Nccompt,itr,err(itr));end
        
        end
        
        % save excitation pattern
        mComp{end}(:,:,idx) = embed(m,maps.mask(:,:,idx));
        
    end

    % calculate final all-slices RMS excitation error
    errComp(end+1) = 1/sqrt(sum(maps.mask(:)))*norm(abs(mComp{end}(maps.mask))-1);
    fprintf('Final %d-channel NRMSE: %0.2f%%.\n',Nccompt,errComp(end)*100);
    
    % calculate total power into the original channels
    rffull = compWtsSv{end}*rfComp{end};
    powComp(end+1) = sqrt(mean(abs(rffull(:)).^2));
    fprintf('Final %d-channel RMS RF amplitude: %f.\n',Nccompt,powComp(end));
    
end

% plot the final error and power as a function of # channels
%figure;
%subplot(121);plot(errcomp*100);xlabel '# Channels';ylabel 'NRMSE (%)';axis square
%subplot(122);plot(powcomp);xlabel '# Channels';ylabel 'RMS RF Amplitude';axis square

